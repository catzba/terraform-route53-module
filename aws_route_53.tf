#-----------------------------------------------------------------------------------------------------
# Route 53
#-----------------------------------------------------------------------------------------------------
#REQUIRED: Get info about hosted zone 
data "aws_route53_zone" "my_domain" {
  name  = var.route53_zone_name
}

#REQUIRED: Create Route 53 record
resource "aws_route53_record" "record" {
  zone_id = data.aws_route53_zone.my_domain.id
  name    = var.domain_name
  type    = var.domain_type

  alias {
    name                   = var.target_dns_name
    zone_id                = var.target_zone_id
    evaluate_target_health = var.evaluate_target_health
  }
}