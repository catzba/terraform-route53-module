#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# Enable VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "route53_zone_name" {
  type    = string
  default = ""
}

variable "domain_name" {
  type    = string
  default = ""
}

variable "domain_type" {
  type    = string
  default = ""
}

# variable "alias" {
#   type    = string
#   default = ""
# }

variable "evaluate_target_health" {
  type    = string
  default = ""
}

variable "target_dns_name" {
  type    = string
  default = ""
}

variable "target_zone_id" {
  type    = string
  default = ""
}